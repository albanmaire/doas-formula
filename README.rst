.. _readme:

doas-formula
============

Formula to install and configure doas.

.. contents:: **Table of contents**
   :depth: 1

General notes
-------------

This formula is written for FreeBSD & Debian.

Special notes
-------------

None.

Documentation
-------------

- See `<pillar-example.sls>`_ for an example. (FreeBSD-based example)
- Fuller documentation on the `wiki <https://gitlab.com/albanmaire/doas-formula/wikis>`_.

Available states
----------------

.. contents::
   :local:

``doas``
^^^^^^^^

*Meta-state (This is a state that includes other states)*.

This installs the doas package and manages the doas configuration file.

``doas.package``
^^^^^^^^^^^^^^^^

This state will install the doas package only.

``doas.config``
^^^^^^^^^^^^^^^

This state will configure the doas application and has a dependency on ``doas.package`` via include list.

``doas.clean``
^^^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

This state will undo everything performed in the ``doas`` meta-state in reverse order, i.e. removes the configuration file and then uninstalls the package.

``doas.config.clean``
^^^^^^^^^^^^^^^^^^^^^

This state will remove the configuration of the doas application.

``doas.package.clean``
^^^^^^^^^^^^^^^^^^^^^^

This state will remove the doas package and has a dependency on ``doas.config.clean`` via include list.

License
-------

| This formula is licensed under the `Apache License 2.0 <https://opensource.org/license/apache-2-0>`_.
| See also `<LICENSE>`_.
