{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as doas with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

doas-config-file-file-managed:
  file.managed:
    - name: {{ doas.config }}
    - source: {{ files_switch(['doas.tmpl.jinja'], lookup='doas-config-file-file-managed') }}
    - template: jinja
    - user: root
    - group: {{ doas.rootgroup }}
    - mode: 0644
    - require:
      - sls: {{ sls_package_install }}
    - context:
        rules: {{ doas.rules }}
