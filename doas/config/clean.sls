{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as doas with context %}

doas-config-clean-file-absent:
  file.absent:
    - name: {{ doas.config }}
