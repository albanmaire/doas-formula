{%- if salt['grains.get']('os_family') in ['FreeBSD', 'Debian'] %}
include:
  - .package
  - .config
{%- else %}
doas:
  test.show_notification:
    - name: Skipping formula execution
    - text: |
        This formula is written for FreeBSD & Debian
        See README for full details (https://gitlab.com/albanmaire/doas-formula)
{%- endif %}
