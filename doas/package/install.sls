{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as doas with context %}

doas-package-install-pkg-installed:
  pkg.installed:
    - name: {{ doas.pkg.name }}
