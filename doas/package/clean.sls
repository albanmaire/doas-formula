{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_clean = tplroot ~ '.config.clean' %}
{%- from tplroot ~ "/map.jinja" import mapdata as doas with context %}

include:
  - {{ sls_config_clean }}

doas-package-clean-pkg-removed:
  pkg.removed:
    - name: {{ doas.pkg.name }}
    - require:
      - sls: {{ sls_config_clean }}
