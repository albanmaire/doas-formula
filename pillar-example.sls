---
doas:
  pkg:
    name: doas
  config: /usr/local/etc/doas.conf
  rootgroup: wheel
  rules:
    - permit alice as root
    - permit keepenv bob as root
